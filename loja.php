<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Loja - Bazar Tem Tudo</title>
		<meta charset="UTF-8"/>
		<link rel="stylesheet" type="text/css" href="style.css"/>
	</head>
	<body>
		<?php
			require_once("./header.php");
		?>
		<table>
			<tr>
				<td>Serrote Serrador</td>
				<td></td>
				<td>Parafusadeira Automatica</td>
			</tr>
			<tr>
				<td><img src="img/serrote.jpg" alt=""></td>
				<td></td>
        <td><img src="img/parafusadeira.jpg" alt=""></td>
			</tr>
			<tr>
				<td>Enxada Puxapuxa</td>
				<td></td>
				<td>Martelo Porrada</td>
			</tr>
			<tr>
				<td><img src="img/enxada.jpg" alt=""></td>
				<td></td>
        <td><img src="img/martelo.jpg" alt=""></td>
			</tr>
		</table>
		<?php
			require_once("./footer.php");
		?>
	</body>
</html>
