<?php
	date_default_timezone_set('America/Sao_Paulo');
	$hora= date("H");
	if($hora<12){
	   $saudacao = "Bom dia!";
	}else if($hora<18){
	   $saudacao="Boa tarde!";
	}else{
	   $saudacao = "Boa noite!";
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>BAZAR TEM TUDO</title>
		<meta charset="UTF-8"/>
		<link rel="stylesheet" type="text/css" href="style.css"/>
	</head>
	<body>
		<?php
			require_once("header.php");
		?>
		<div class="container">
			<br>
			<span><?php echo $saudacao ?></span>
			<br><br>
			<span>Acesse a nossa <a href="loja.php">loja</a> para ver nossos produtos!</span>
		</div>
		<?php
			require_once("footer.php");
		?>
	</body>
</html>
